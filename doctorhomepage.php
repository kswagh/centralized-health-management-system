<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
			  -->
		<script src="jquery-3.3.1.min.js" ></script>
		<link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> 
	 
		<script src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/msc-script.js"></script>

		<style type="text/css">
			* {box-sizing: border-box;}

			body { 
			  margin: 0;
			  font-family: Arial, Helvetica, sans-serif;
			}
			/*footer*/
			.content {
			  min-height: 100%;
			}
			/*card*/
			.card{
				margin-top: 10px;
				margin-bottom: 20px;
			}


			/*.header {
				overflow: hidden;
				background-color: #f1f1f1;
				padding: 20px 10px;
			}

			.header a {
				float: left;
				color: black;
				text-align: center;
				padding: 12px;
				text-decoration: none;
				font-size: 18px; 
				line-height: 25px;
				border-radius: 4px;
			}

			.header a.logo {
				font-size: 25px;
				font-weight: bold;
			}

			.header a:hover {
				background-color: #ddd;
				color: black;
			}
		  	.header-right {
		   	 float: right;
		  	}*/
		  	/*table*/
			.row{
			    /*margin-top:40px;*/
			    padding: 0 10px;
			}
			.clickable{
			    cursor: pointer;   
			}

			.panel-heading div {
				margin-top: -18px;
				font-size: 15px;
			}
			.panel-body{
				display: none;
			}  

			.thread-dark{

			}

		</style>
      	<script>
      		$(document).ready(function(){
      			$.ajax({
					type: "GET",
					url: "getdocaptmts.php",
					success: function(response){
					    var todaysaptmts = JSON.parse(response);
					    //var cls = "btnsle" 
					        $.each(todaysaptmts, function(i,data) {
					            $("#tabslots").append("<tr><th>"+i+"</th><td style='display:none;'>"+data.drsltID+"</td><td>" + data.sltdt + "</td><td>"+data.ptnrgnID+"</td><td>"+data.ptntname+"</td><td>"+data.reason+"</td><td>"+data.sltm+"</td><td><button type='button' id='btn"+i+"' onclick='showdata(this.id)' class='btn btn-success btn-sm'> " + "Proceed" + "</button></td></tr><br/>");
					        });
					}
				})

      		});	
	
      			


      	</script>	

	</head>

	<body>

		<div class="content">	
		<!-- first navbar -->
		<nav class="navbar navbar-inverse" style="background-color: 2F4F4F;">
	 		<ul class="navbar-nav">
    			<li class="nav-item" >
    				<a class="navbar-brand" href="#">
						<img src="img/healthcarelogo.jpg" alt="Logo" style="width:40px;">
					</a>
		    		<a href="#home" >LUPUS HEALTHCARE</a>
		  		</li>
		  	</ul>
		  	<ul class="nav navbar-nav navbar-right">	
		  		<li class="nav-item">		
			   		<a style="color: white;"><?php $uname=$_COOKIE['un']; echo"Dr ".$uname; ?></a>
					<button class="btn btn-warning btn-sm" onclick="logout()" >Logout</button>
				</li>
			</ul>		
		 </nav>

		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="card">
						<header class="card-header card-header-danger" >	
								<h3 class="card-title" >Todays Appointments</h3>
						</header>		
							<div class="card-body">
								<div class="table-responsive">			
								<table class="table table-hover table-striped" id="tabslots">
									<thead class="thead-dark">
									<tr>
										<th >#</th>
										<th>Date</th>
										<th>Reg Id</th>
										<th>Name</th>
										<th>Reason</th>
										<th>Time</th>
										<th></th>
									</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
							</div>
					</div>
				</div>
			</div>						
		</div>
	</div>	
	<footer class="footer page-footer font-small blue " style="background-color: 2F4F4F; ">
	  <div class="footer-copyright text-center py-3">© 2018 Copyright:
	    <a > Lupus Healthcare</a>
	  </div>
	</footer>
	

		<script>
	//		loadtodaysappointments();

			function logout() {
				if (confirm('Are you sure you want to logout?')) {
					window.location.replace("destroysession.php");
				}					 
			}
	

				function showdata(id) { 
						var btnval = document.getElementById(id);
						 var $row = $(btnval).closest("tr");
						  $tds1 = $row.find("td:nth-child(2)");//sltid
						  $tds4 = $row.find("td:nth-child(5)");//name
						 // alert($tds1.text()+$tds4.text());
						var answer = confirm("Do you want to proceed treatment for "+$tds4.text()+"?");
						if(answer==true) {

							$.ajax({
								type: "GET",
								url: "dochomepageproceed.php",
								data: {sltid: $tds1.text(), nm: $tds4.text()},
								success: function(response){
								    window.location.replace("docpatientsymp.php");
								}
							});
						}
					    
				}
		</script>			
	</body>
</html>