<html>
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	 <script src="jquery-3.3.1.min.js" >
	</script>
		<script src="jquery-3.3.1.min.js" >
		</script>

		<link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> 
	 
		<script src="js/bootstrap.min.js">
		</script>

		</script>
			<style>


			 	/*for nav bar*/
		  	body {
		  	margin: 0;
			font-family: Arial, Helvetica, sans-serif;
			}
			/*for footer*/
			.content {
			  min-height: 100%;
			}
			/*symp/pres/bill.... tabs*/
			.nav-tabs>li>a {
			 color: #ffffff  ;
			}
			.nav>li>a:focus, .nav>li>a:hover{
			    background-color: #1c2331;
			}
			.nav-tabs{
			    background-color: #33b5e5; 
			}

			/*card*/
			.card{
				margin-top: 10px;
				margin-bottom: 20px;
			}
			/*.row-bottom-margin { margin-bottom:20px; }*/

			/*table*/
			.row{
			    /*margin-top: 40px;*/
			    padding: 0 10px;
			}
			.clickable{
			    cursor: pointer;   
			}

			.panel-heading div {
				margin-top: -18px;
				font-size: 15px;
			}
			.panel-body{
				display: none;
			}  

			.thread-dark{

			}

		</style>
	</head>	


	<body >

		<div class="content">	
		<!-- first navbar -->
		<nav class="navbar navbar-inverse" style="background-color: 2F4F4F;">
	 		<ul class="navbar-nav">
    			<li class="nav-item" >
    				<a class="navbar-brand" href="#">
						<img src="img/healthcarelogo.jpg" alt="Logo" style="width:40px;">
					</a>
		    		<a href="#home" >LUPUS HEALTHCARE</a>
		  		</li>
		  	</ul>
		  	<ul class="nav navbar-nav navbar-right">	
		  		<li class="nav-item">		
			   		<a style="color: white;"><?php $uname=$_COOKIE['un']; echo $uname; ?></a>
					<button class="btn btn-warning btn-sm" onclick="logout()" >Logout</button>
				</li>
			</ul>		
		 </nav>	


		<!-- nav tabs -->	
		<ul class="nav nav-tabs nav-justified">	
		  <li class="nav-item "><a class="nav-link active" href="patienthomepage.php">Book Appointments</a></li>
		  <li class="nav-item"><a class="nav-link " href="patientsymp.php">Symptoms</a></li>
		  <li class="nav-item"><a class="nav-link " href="patienttst.php">Test Reports</a></li>
		  <li class="nav-item"><a class="nav-link " href="patientdiag.php">Diagnosis</a></li>
		  <li class="nav-item"><a class="nav-link " href="patientpres.php">Prescription</a></li>
		  <li  class="nav-item" ><a class="nav-link " href="patientpay.php">Billing</a></li>
	 	</ul> 


	 	<div class="container">
				<div class="row ">
				  	<div class="col-sm-5">
				    	<div class="card">
				      		<div class="card-body">
				      			<div class="form-group">
				      				<div class="col-6">
									<!-- reason input -->
										<label><b>Reason:</b></label><br>
									</div>	
									<div class="col-12">
										<input type="text" id="txtreason" class="form-control" pattern="[A-Za-z]{1,}"><br>
									</div>
									<div class="col-6">
										<!--Fill Speciality -->
										<label><b>Select Speciality:</b></label><br>
									</div>
									<div class="col-7">	
										<select id="specselect" onchange="loaddoctors()" class="form-control">
											 <option value="" selected disabled hidden>Choose Speciality</option>
										</select><br>
									</div>	
									<div class="col-6">
										<!--Fill Doctors names-->
										<label><b>Select Doctor:</b></label><br>
									</div>
									<div class="col-7">	
										<select id="docselect" onchange="loaddate()" class="form-control">
											 <option value="" selected disabled hidden>Choose Doctor</option>
										</select><br>
									</div>
									<div class="col-6">
										<label><b>Select Suitable Date:</b></label><br>
									</div>
									<div class="col-6">	
										<select id="docaptmtdate" class="form-control">
											<option value="" selected disabled hidden>Choose Date</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
						<div class="col-sm-7">
					    	<div class="card">
					    		<header class="card-header card-header-danger" >	
									<h3 class="card-title" >Available appiontments</h3>
								</header>
						      		<div class="card-body">			
										<div id="tabslots" class="table-responsive"></div>
										<!-- 	<table class="table table-hover table-striped" name="ts" id="tabslots">
												<thead class="thead-dark">					
												</thead>
												<tbody>
													
												</tbody>
											</table> -->
									</div>
								</div>
							</div>
						</div>
					<div class="row ">
					  	<div class="col-sm-5">
					    	<div class="card">
					      		<div class="card-body">
					      			<div class="panel panel-primary">
										<div class="panel-heading">
											<h3 class="panel-title">Upcoming Appointments</h3>
										</div>
										<div class="col-6">
										<label  style="color:red;"><b>**</b></label><br>
										</div>
										<div class="col-12">
										<p id ="upcomingaptmsid"></label><br>
										</div>
									</div>		
					      		</div>
					      	</div>
					     </div>
					</div>		      			
						
			</div>

		</div>															

	<footer class="footer position-relative page-footer font-small blue " style="background-color: 2F4F4F; ">
	  <div class="footer-copyright text-center py-3">© 2018 Copyright:
	    <a href="https://mdbootstrap.com/education/bootstrap/"> Lupus Healthcare</a>
	  </div>
	</footer>		
		
		<!-- <button id="reset">Reset</button> -->
		<script type="text/javascript">
			var flag1=0;
			/*var clonedt;
			var cloneslttab;*/
			loadspeciality();
			loadupcomingaptms();	

			function reset () {
				 $('#docaptmtdate')
				    .empty()
				;
				return;
			}
			/*logout function*/
			function logout() {
				window.location.replace("destroysession.php");				 
			}

			/*function for loading Speciality at form load*/
			function loadspeciality(){

				var obj = new XMLHttpRequest();
				obj.open("GET", "getspecfilled.php", true);
				obj.send();

				obj.onreadystatechange = function() {
					if(obj.status == 200 && obj.readyState == 4) {
						var myObj = JSON.parse(obj.responseText);

						
						var select = document.getElementById('specselect');
						
						for (let specname in myObj) {
							
							newOption = document.createElement('option');
						    newOption.value=myObj[specname];
						    newOption.text=myObj[specname];
						    select.appendChild(newOption);

					
							
						}
					}

				}
			}

			/*function for filling docselect(doctors)*/
			function loaddoctors() {

				$('#docselect')
				    .empty()
				    .append('<option value="" selected disabled hidden>Choose Doctor</option>')
				;
				

				var sname = document.getElementById("specselect").value;
				var obj = new XMLHttpRequest();
				obj.open("GET", "getdocfilled.php?sname="+sname, true);
				obj.send();

				obj.onreadystatechange = function() {
					if(obj.status == 200 && obj.readyState == 4) {
						var myObj = JSON.parse(obj.responseText);

						
						var select = document.getElementById('docselect');
						
						for (let docname in myObj) {
							//new Option('myObj[docname]','myObj[docname]');
							
							newOption = document.createElement('option');
						    newOption.value=myObj[docname];
						    newOption.text=myObj[docname];
						    select.appendChild(newOption);

					
							
						}
					}

				}
			}

			/*function for loading doctors availability dates */
			function loaddate() {
					//reset();
					$('#tabslots').find("tr").remove();

					$('#docaptmtdate')
					    .empty()
					    .append('<option value="" selected disabled hidden>Choose Date</option>')
					;
/*					$("#docaptmtdate").replaceWith(clonedt.clone());
					$("#tabslots").replaceWith(cloneslttab.clone());*/
				
				//document.getElementById("docaptmtdate").options.length = 0;
				//document.getElementById("docaptmtdate").options.length = 0;
				//$("#tabslots").remove(); 

				var dname = document.getElementById("docselect").value;
				var obj = new XMLHttpRequest();
				obj.open("GET", "getdatesfilled.php?dname="+dname, true);
				obj.send();

				obj.onreadystatechange = function() {

					if(obj.status == 200 && obj.readyState == 4) {
						var myObj = JSON.parse(obj.responseText);

						
						var select = document.getElementById('docaptmtdate');

						for (let ddate in myObj) {
							//new Option('myObj[docname]','myObj[docname]');
							
							newOption = document.createElement('option');
						    newOption.value=myObj[ddate];
						    newOption.text=myObj[ddate];
						    select.appendChild(newOption);

					
							/*document.getElementById("docselect").innerHTML = "<option value=\""+myObj[docname]+"\">"+myObj[docname]+"</option>";*/
						}
					}

				}
			}

				/*Function for loading upcoming appointments*/	
				function loadupcomingaptms(){
					$.ajax({
						type: "GET",
						url: "patientupcomingapptms.php",
						success: function(response){
							var aptmsdata="";
							var aptms = JSON.parse(response);	
							 $.each(aptms, function(i,data) {
							 	aptmsdata += "<div style='color:red;'>Appointment "+i+":</div>" + "Dr Name: " + data.drnm + "<br>"+" Speciality: " + data.drspeclty + "<br>" + " Date: " + data.sltdt + "<br>" + " Time: " + data.sltm + "<br>"+"<button type='button' id='btn"+i+"' onclick='cancelaptmt("+data.drsltid+")' class='btn btn-success btn-sm'> " + "Cancel" + "</button>";
							 });	
							 	document.getElementById("upcomingaptmsid").innerHTML = aptmsdata; 
						}	
					});
				}

			/*function for canceling the appointment*/
				function cancelaptmt(slid) {
						var answer = confirm("Do you want to cancel the appointment?");
						if(answer==true) {
							$.ajax({
								type: "GET",
								url: "cancelaptmt.php",
								data: {sltid: slid},
								success: function(response){
								    alert("Your appointment has been canceled!");
								     loadupcomingaptms();
								}
							});
						}
					    
				}	

			/*function for loading table with available slots */
			$(document).ready(function(){

				/*clonedt = $("#docaptmtdate").clone();
				cloneslttab = $("#tabslots").clone();*/
				$('#docaptmtdate').on('change',function(event){

					var dname = document.getElementById("docselect").value;
					var seldt = document.getElementById("docaptmtdate").value;

					$('#tabslots').find("tr").remove();	

					$.ajax({
						type: "GET",
						url: "getslotsfilled.php",
						data: {dname: dname, date: seldt},
						success: function(response){
						    var slots = JSON.parse(response);
						    	var table = $("<table />");
						    	//document.getElementById("docselect").
						    	var row = $(table[0].insertRow(-1));

						        $.each(slots, function(i,data) {
						        	if(i==8) {
						        		row = $(table[0].insertRow(-1));
						        	}						        
						        	var cell = $("<td />");

									var lastIndex = data.lastIndexOf(":");
									var s1 = data.substring(0, lastIndex);

						        	cell.html("<button id='butn"+i+"' class='btn btn-info btn-lg ' value='"+s1+"' onclick='btnsel(this.id)'>" + s1 + "</button></td>");
						        	row.append(cell);

						            //row.append("<td><button class='btn' value=\""+data+"\"onclick='btnsel()'>" + data + "</button></td><br>");
						        });
						        var dvTable = $("#tabslots");
						        dvTable.html("");
						        dvTable.append(table);
						        document.getElementById("upcomingaptmsid").innerHTML = ""
						        loadupcomingaptms();
						  	//$("#tabslots")
						}
					});
				});
			});

			/*function for booking appointment with selected button slot*/
			function btnsel(id){
				//var validator = require('validator');
				var reason = document.getElementById("txtreason").value;
				/*for clearing the table*/
				if(reason.match(/^[a-zA-Z\s]*$/))
				{

				$('#tabslots').find("tr:gt(0)").remove();
				var dname = document.getElementById("docselect").value;
				var seldt = document.getElementById("docaptmtdate").value
				//var reason = document.getElementById("txtreason").value;
				var btnval = document.getElementById(id).value;
				/*if (btnval.length > 0) {*/
					var answer = confirm("Confirm appointment?\nTime: "+btnval+"\nDoctorname: "+dname +"\nDate:"+seldt)
					
					if (answer==true) {

						$.ajax({
							type: "GET",
							url: "bookappointment.php",
							data: {dname: dname, date: seldt, aptmttime: btnval, rsn:reason},
							success: function(response){
							    alert("Your appointment has been confirmed! :)");
							    window.location.replace("patienthomepage.php");

							    //$("#tabslots").append("<td><button class='btn' value=\""+data+"\"onclick='btnsel()'>" + data + "</button></td>");
							}
						});
					    
					}
				}else {

					alert("Invalid fields-Reason:pattern[a-zA-Z]" );
				}	
		}
			
		/*	$(document).on("click", 'ul li', function(){
			    $('ul li').removeClass('active');
			    $(this).addClass('active');
			});*/
		</script>	
	</body>	
</html>