<?php
	session_start();

	$filter = $_GET['fltr'];
	$conn = new mysqli("localhost","root","","project");

	if($conn->connect_error) {
		die("Connection failed".$conn->connect_error);
	}

	$return_arr = array();
	$x=1;

	$patregid=$_COOKIE['ptntregid'];
	$username=$_COOKIE['un'];

	if($filter=="MyName"){
		$sqlselectdiaginstrcn="select ds.sltdt,pd.diagname,pd.instrnEng,dr.drnm
							from doctorslot ds, doctor dr, ptntdiagnosis pd
							where  dr.usrname = '$username' 
							and ds.drrgnID= dr.drrgnID
							and ds.ptnrgnID=$patregid
							and ds.status in ('I','C')	
							and pd.drsltID = ds.drsltID		
							order by ds.drsltID desc";


		$result1 = $conn->query($sqlselectdiaginstrcn);

		if (!$result1) {
	    trigger_error('Invalid query: ' . $conn->error);
		}

		if ($result1->num_rows > 0) {
	       while($obj = $result1->fetch_object()) {
	       		$return_arr[$x]["sltdt"] = $obj->sltdt;
		       	$return_arr[$x]["diagnm"] = $obj->diagname;
		       	$return_arr[$x]["inceng"] = $obj->instrnEng;
		       	$return_arr[$x]["drnm"] = $obj->drnm;
		       	$x++;
	    	}
	    }

	    $myJSON = json_encode($return_arr);
		print_r($myJSON);

	} else if($filter=="MySpeciality"){
		$sqlselectdiaginstrcn="select ds.sltdt,pd.diagname,pd.instrnEng,dr.drnm
							from doctorslot ds, doctor dr, ptntdiagnosis pd
							where dr.drspeclty=(select drspeclty from doctor where usrname='$username') 
							and ds.drrgnID= dr.drrgnID
							and ds.ptnrgnID=$patregid
							and ds.status in ('I','C')	
							and pd.drsltID = ds.drsltID		
							order by ds.drsltID desc";


		$result1 = $conn->query($sqlselectdiaginstrcn);

		if (!$result1) {
	    trigger_error('Invalid query: ' . $conn->error);
		}

		if ($result1->num_rows > 0) {
	       while($obj = $result1->fetch_object()) {
	       		$return_arr[$x]["sltdt"] = $obj->sltdt;
		       	$return_arr[$x]["diagnm"] = $obj->diagname;
		       	$return_arr[$x]["inceng"] = $obj->instrnEng;
		       	$return_arr[$x]["drnm"] = $obj->drnm;
		       	$x++;
	    	}
	    }

	    $myJSON = json_encode($return_arr);

		print_r($myJSON);
	}	

?>
