<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<script src="jquery-3.3.1.min.js" >
		</script>

		<link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> 
	 
		<script src="js/bootstrap.min.js">
		</script>

		<style type="text/css">

		  	/*for nav bar*/
		  	body {
		  		margin: 0;
			  font-family: Arial, Helvetica, sans-serif;
			}
			.content {
			  min-height: 100%;
			}
			/*symp/pres/bill.... tabs*/
			.nav-tabs>li>a {
			 color: #ffffff  ;
			}
			.nav>li>a:focus, .nav>li>a:hover{
			    background-color: #1c2331;
			}
			.nav-tabs{
			    background-color: #33b5e5; 
			}

			/*card*/
			.card{
				margin-top: 10px;
				margin-bottom: 20px;
			}

			/*table*/
			.row{
			   /* margin-top:40px;*/
			    padding: 0 10px;
			}
			.clickable{
			    cursor: pointer;   
			}

			.panel-heading div {
				margin-top: -18px;
				font-size: 15px;
			}
			.panel-body{
				display: none;
			}  

			.thread-dark{

			}

		</style>	
	<head>	
	<body>
		<div class="content">
		<!-- first navbar -->
		<nav class="navbar navbar-inverse" style="background-color: 2F4F4F;">
	 		<ul class="navbar-nav">
    			<li class="nav-item" >
    				<a class="navbar-brand" href="#">
						<img src="img/healthcarelogo.jpg" alt="Logo" style="width:40px;">
					</a>
		    		<a href="#home" >LUPUS HEALTHCARE</a>
		  		</li>
		  	</ul>
		  	<ul class="nav navbar-nav navbar-right">	
		  		<li class="nav-item">		
			   		<a style="color: white;"><?php $uname=$_COOKIE['un']; echo"Dr ".$uname; ?></a>
					<button class="btn btn-warning btn-sm" onclick="logout()" >Logout</button>
				</li>
			</ul>		
		 </nav>
		  
		 <!-- nav tabs -->	
		<ul class="nav nav-tabs nav-justified">
		  <li class="nav-item"><a class="nav-link " href="docpatientsymp.php">Symptoms</a></li>
		  <li class="nav-item"><a class="nav-link " href="docpatienttst.php">Test Reports</a></li>
		  <li class="nav-item"><a class="nav-link active" href="docpatientdiag.php">Diagnosis</a></li>
		  <li class="nav-item"><a class="nav-link " href="docpatientpres.php">Prescription</a></li>
		  <li  class="nav-item" ><a class="nav-link " href="docpatientpay.php">Billing</a></li>
	 	</ul> 

	 	<!-- second navbar -->
	 	<nav class="navbar navbar-inverse">
	 		<ul class="navbar-nav">
    			<li class="nav-item">
    				<button onclick="bcktoaptmts()" class="btn btn-default btn-sm">Back To Appointments</button>
    			</li>
    		</ul>
    		<ul class="nav navbar-nav navbar-right">	
    			<li class="nav-item" >
    				<button class="btn btn-light btn-sm">Patient Name: <?php $patname = $_COOKIE['patname']; echo $patname; ?></button>	
    			</li>
    		</ul>
    	</nav>	
		
		
		<!-- input and table fill diagnosis -->
		<div class="container">
				<div class="row">
				  	<div class="col-sm-5">
				    	<div class="card">
				      		<div class="card-body">
				      			<div class="form-group">
				      				<div class="col-6">
										<label><b>Diagnosis:</b></label><br/>
									</div>
									<div class="col-7">	
										<select id="diag"  class="form-control">
											<option value="" selected disabled hidden >Choose Diagnosis</option>
										</select>
									</div>
									<div class="col-12">	
										<textarea name="txtdiag" rows="2" cols="50" id="diagid" class="form-control"></textarea>
									</div>
									<div class="col-6">
										<label><b>Instruction:</b></label><br/>
									</div >
									<div class="col-7">	
										<select id="instrcn" class="form-control">
											<option value="" selected disabled hidden >Choose Instruction</option>
										</select>					
									</div>
									<div class="col-12">	
										<textarea name="txtinstrcn" rows="2" cols="50" id="instrcnid" class="form-control"></textarea>
									</div><br>
									<div class="col-6">	
										<button type="button" id="btnsave" class="btn btn-primary btn-md">Save</button>	
									</div>

							</div>
						</div>
					</div>
				</div>	
					<div class="col-sm-7">
				    	<div class="card">
				    		<header class="card-header card-header-danger" >	
								<h3 class="card-title" >Past Diagnosis Record</h3>
							</header>
				      		<div class="card-body">
				      			<div class="col-7">
									<select id="filterselectid" class="form-control" onchange="loadfilter()">
										<option value="" selected disabled hidden>Choose Filter</option>
										<option value="">All</option>
										<option value="">MyName</option>
										<option	value="">MySpeciality</option>
									</select>
								</div>
								<div class="table-responsive">				
								<table class="table table-hover table-striped" id="tabfilldiag">
									<thead class="thead-dark">
										<tr>
											<th>#</th>
											<th>Date</th>
											<th>Diagnosis</th>
											<th>Instruction</th>
											<th>Doctor</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	

	
	<footer class="footer page-footer font-small blue position-relative" style="background-color: 2F4F4F; ">
	  <div class="footer-copyright text-center py-3">© 2018 Copyright:
	    <a href="https://mdbootstrap.com/education/bootstrap/"> Lupus Healthcare</a>
	  </div>
	</footer>						
							
				      					
		<script>
			loadselectdiag();
			loadselectinstrcn();
			loadtabdiag();

			function logout() {
				if (confirm('Are you sure you want to logout?')) {
					window.location.replace("destroysession.php");
				}					 
			}

			function bcktoaptmts() {
				window.location.replace("clearcookiesbktoaptmt.php");	
			}
	

			/*load diagnosis select dropdown*/
			function loadselectdiag() {
				var obj = new XMLHttpRequest();
				obj.open("GET", "getdiagfilled.php", true);
				obj.send();

				obj.onreadystatechange = function() {
					if(obj.status == 200 && obj.readyState == 4) {
						var myObj = JSON.parse(obj.responseText);
			
						
						var select = document.getElementById('diag');
						
						for (let diagname in myObj) {
							//new Option('myObj[diagname]','myObj[diagname]');
							
							newOption = document.createElement('option');
						    newOption.value=myObj[diagname];
						    newOption.text=myObj[diagname];
						    select.appendChild(newOption);

					
							/*document.getElementById("docselect").innerHTML = "<option value=\""+myObj[docname]+"\">"+myObj[docname]+"</option>";*/
						}
					}

				}
			}

			//fill instruction select tag with masters data
			function loadselectinstrcn() {
				var obj = new XMLHttpRequest();
				obj.open("GET", "getinstrnfilled.php", true);
				obj.send();

				obj.onreadystatechange = function() {
					if(obj.status == 200 && obj.readyState == 4) {
						var myObj = JSON.parse(obj.responseText);
			
						
						var select = document.getElementById('instrcn');
						
						for (let instrcnname in myObj) {
							//new Option('myObj[diagname]','myObj[diagname]');
							
							newOption = document.createElement('option');
						    newOption.value=myObj[instrcnname];
						    newOption.text=myObj[instrcnname];
						    select.appendChild(newOption);

					
							/*document.getElementById("docselect").innerHTML = "<option value=\""+myObj[docname]+"\">"+myObj[docname]+"</option>";*/
						}
					}

				}
			}

			/*function for filter*/
			function loadfilter(){
				 var filter = $('#filterselectid').find('option:selected').text();
				 if(filter==="All") {
				 	$('#tabfilldiag').find("tr:gt(0)").remove();
				 $.ajax({
						type: "GET",
						url: "loaddiag.php",				
						success: function(response){
						    var diagdet = JSON.parse(response);
						        $.each(diagdet, function(i,data) {				
						            $("#tabfilldiag").append("<tr><th>"+i+"</th><td>" + data.sltdt + "</td><td>"+data.diagnm+"</td><td>"+data.inceng+"</td><td>"+data.drnm+"</td></tr><br/>");
						        });
						}
					});
				 } else if (filter==="MyName") {
				 	$('#tabfilldiag').find("tr:gt(0)").remove();
					$.ajax({
						type: "GET",
						url: "loadfilterdiag.php",
						data: {fltr: filter},					
						success: function(response){
						    var diagdet = JSON.parse(response);
						        $.each(diagdet, function(i,data) {				
						            $("#tabfilldiag").append("<tr><th>"+i+"</th><td>" + data.sltdt + "</td><td>"+data.diagnm+"</td><td>"+data.inceng+"</td><td>"+data.drnm+"</td></tr><br/>");
						        });
						}
					});		
				 } else if (filter==="MySpeciality") {
				 	$('#tabfilldiag').find("tr:gt(0)").remove();
				 	$.ajax({
						type: "GET",
						url: "loadfilterdiag.php",
						data: {fltr: filter},					
						success: function(response){
						    var diagdet = JSON.parse(response);
						        $.each(diagdet, function(i,data) {				
						            $("#tabfilldiag").append("<tr><th>"+i+"</th><td>" + data.sltdt + "</td><td>"+data.diagnm+"</td><td>"+data.inceng+"</td><td>"+data.drnm+"</td></tr><br/>");
						        });
						}
					});
				 }					 
			}		


			//onload fill table with past diagnosis records
			function loadtabdiag() {	
					$.ajax({
						type: "GET",
						url: "loaddiag.php",				
						success: function(response){
						    var diagdet = JSON.parse(response);
						        $.each(diagdet, function(i,data) {				
						            $("#tabfilldiag").append("<tr><th>"+i+"</th><td>" + data.sltdt + "</td><td>"+data.diagnm+"</td><td>"+data.inceng+"</td><td>"+data.drnm+"</td></tr><br/>");
						        });
						}
					});
			}
		
				$('#diag').on('change',function(event){
					//alert($('#symp').find('option:selected').text());
					//$('#txtsym').val("")
					 var info = $('#diag').find('option:selected').text();
  					$("#diagid").append(info+"\n");
				});

				$('#instrcn').on('change',function(event){
					//alert($('#symp').find('option:selected').text());
					//$('#txtsym').val("")
					 var info = $('#instrcn').find('option:selected').text();
  					$("#instrcnid").append(info+"\n");
				});

				$('#btnsave').click(function(event){
					$('#tabfilldiag').find("tr:gt(0)").remove();
					var diag = document.getElementById('diagid').value;
					var instrcn = document.getElementById('instrcnid').value;
					if(diag.match(/^[a-zA-Z][a-zA-Z\s0-9/&:,+()-]*$/) && instrcn.match(/^[a-zA-Z][a-zA-Z\s0-9/&:,]*$/))
					{
						if(diag=="" ) {
							alert("Fields empty! First fill all fields.");

						} else {
							var answer = confirm("Do you want to save Diagnosis details?");
								if(answer==true) {
									$.ajax({
										type: "GET",
										url: "savediag.php",
										data: {txtdiag: diag, txtinstrcn: instrcn},
										success: function(response){
		/*
									    	$("#tabslots").append("<tr><th>"+"Date"+"</th><th>"+"Symptoms"+"</th><th>"+"Diagnosis"+"</th><th>"+"Doctor"+"</th></tr>");*/

										    var diagdet = JSON.parse(response);
										        $.each(diagdet, function(i,data) {
										            $("#tabfilldiag").append("<tr><th>"+i+"</th><td>" + data.sltdt + "</td><td>"+data.diagnm+"</td><td>"+data.inceng+"</td><td>"+data.drnm+"</td></tr><br/>");
										        });

										        $('#diagid').val('');
										        $('#instrcnid').val('');
										}
									});
								} else {
									loadtabdiag();
								}
						}
					} else {
						alert("Invalid Fields: Diagnosis or Instructions!");
					}			
				});
		</script>	
	</body>
</html>			
