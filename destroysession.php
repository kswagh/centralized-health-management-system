<?php
	session_start();
	setcookie("un", '', time() - 3600);
	setcookie("date", '', time() - 3600);
	setcookie("regno", '', time() - 3600);
	setcookie("name", '', time() - 3600);
	setcookie("aptime", '', time() - 3600);
	setcookie("sltid", '', time() - 3600);
	setcookie("ptntregid", '', time() - 3600);
	setcookie("patname", '', time() - 3600);
 	unset($_SESSION['uname']);
 	session_destroy();
	header('location:patientlogin.html');
?>