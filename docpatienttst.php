<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<script src="jquery-3.3.1.min.js" >
		</script>

		<link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> 
	 
		<script src="js/bootstrap.min.js">
		</script>

		<style type="text/css">

		  	/*for nav bar*/
		  	body {
		  		margin: 0;
			  font-family: Arial, Helvetica, sans-serif;
			}
			/*for footer*/
			.content {
			  min-height: 100%;
			}
			/*symp/pres/bill.... tabs*/
			.nav-tabs>li>a {
			 color: #ffffff  ;
			}
			.nav>li>a:focus, .nav>li>a:hover{
			    background-color: #1c2331;
			}
			.nav-tabs{
			    background-color: #33b5e5; 
			}

			/*card*/
			.card{
				margin-top: 10px;
				margin-bottom: 20px;
			}

			/*table*/
			.row{
			    /*margin-top:40px;*/
			    padding: 0 10px;
			}
			.clickable{
			    cursor: pointer;   
			}

			.panel-heading div {
				margin-top: -18px;
				font-size: 15px;
			}
			.panel-body{
				display: none;
			}  

			.thread-dark{

			}
		</style>
	
	<head>	
	<body>
		<div class="content">
		<!-- first navbar -->
		<nav class="navbar navbar-inverse" style="background-color: 2F4F4F;">
	 		<ul class="navbar-nav">
    			<li class="nav-item" >
    				<a class="navbar-brand" href="#">
						<img src="img/healthcarelogo.jpg" alt="Logo" style="width:40px;">
					</a>
		    		<a href="#home" >LUPUS HEALTHCARE</a>
		  		</li>
		  	</ul>
		  	<ul class="nav navbar-nav navbar-right">	
		  		<li class="nav-item">		
			   		<a style="color: white;"><?php $uname=$_COOKIE['un']; echo"Dr ".$uname; ?></a>
					<button class="btn btn-warning btn-sm" onclick="logout()" >Logout</button>
				</li>
			</ul>		
		 </nav>

		 <!-- nav tabs -->	
		<ul class="nav nav-tabs nav-justified">
		  <li class="nav-item"><a class="nav-link " href="docpatientsymp.php">Symptoms</a></li>
		  <li class="nav-item"><a class="nav-link active" href="docpatienttst.php">Test Reports</a></li>
		  <li class="nav-item"><a class="nav-link " href="docpatientdiag.php">Diagnosis</a></li>
		  <li class="nav-item"><a class="nav-link " href="docpatientpres.php">Prescription</a></li>
		  <li  class="nav-item" ><a class="nav-link " href="docpatientpay.php">Billing</a></li>
	 	</ul> 

	 	<!-- second navbar -->
	 	<nav class="navbar navbar-inverse">
	 		<ul class="navbar-nav">
    			<li class="nav-item">
    				<button onclick="bcktoaptmts()" class="btn btn-default btn-sm">Back To Appointments</button>
    			</li>
    		</ul>
    		<ul class="nav navbar-nav navbar-right">	
    			<li class="nav-item" >
    				<button class="btn btn-light btn-sm">Patient Name: <?php $patname = $_COOKIE['patname']; echo $patname; ?></button>	
    			</li>
    		</ul>
    	</nav>	
		
		
		<!-- symptoms and table  cards-->
			<div class="container">
				<div class="row">
				  	<div class="col-sm-5">
				    	<div class="card">
				      		<div class="card-body">
				      			<div class="form-group">
				      				<div class="col-6">
				      					<label><b>Select Test:</b></label>
				      				</div>
				      				<div class="col-12">		
										<select id="test" class="form-control">
											<option value="" selected disabled hidden>Select Test</option>
										</select><br>
									</div>
									<div class="col-6">		
										<button type="button" id="btnaddrpt" class="btn btn-primary btn-md">Add Report</button>	
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-7">
				    	<div class="card">
				    		<header class="card-header card-header-danger" >	
								<h3 class="card-title" >Past Test Records</h3>
							</header>
				      		<div class="card-body">
				      			<div class="table-responsive">			
								<table class="table table-hover table-striped" id="tabfilllbtst">
									<thead class="thead-dark">
										<tr>
											<th>#</th>
											<th>Date</th>
											<th>Test Name</th>
											<th>Doctor</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>									
													
	<footer class="footer page-footer font-small blue position-relative" style="background-color: 2F4F4F; ">
	  <div class="footer-copyright text-center py-3">© 2018 Copyright:
	    <a href="https://mdbootstrap.com/education/bootstrap/"> Lupus Healthcare</a>
	  </div>
	</footer>	

		<script>
			loadselecttest();
			loadtabtest();

			function logout() {
				if (confirm('Are you sure you want to logout?')) {
					window.location.replace("destroysession.php");
				}					 
			}
			function bcktoaptmts() {
				window.location.replace("clearcookiesbktoaptmt.php");	
			}

			function bcktoaptmts() {
				window.location.replace("clearcookiesbktoaptmt.php");	
			}
	

			/*load test select dropdown*/
			function loadselecttest() {
					$.ajax({
						type: "GET",
						url: "gettestsfilled.php",				
						success: function(response){
							var select = document.getElementById('test');
						    var testdet = JSON.parse(response);
						        $.each(testdet, function(i,data) {
						    		newOption = document.createElement('option');
								    newOption.value=data.tst;
								    newOption.text=data.tst;
								    select.appendChild(newOption);    	
						        });
						}
					});

			}


			//onload fill table with past prescription records
			function loadtabtest() {	

					$.ajax({
						type: "GET",
						url: "loadtest.php",				
						success: function(response){
								$("#tabslots").append("<tr><th>"+"Date"+"</th><th>"+"Report Name"+"</th><td>"+"Slot Time"+"</td><th>"+"Doctor"+"</th></tr>");

						    var testdet = JSON.parse(response);
						        $.each(testdet, function(i,data) {				
						            $("#tabfilllbtst").append("<tr><th>"+i+"</th><td style='display:none;'>"+data.drsltid+"</td><td>" + data.sltdt + "</td><td>"+data.lbtstname+"</td><td>"+data.drnm+"</td><td><button type='button' id='btn"+i+"' onclick='showdata(this.id)' class='btn btn-primary btn-md'> " + "See Report" + "</button></td></tr><br/>");
						        });
						}
					});
			}

				//add new report for the patient
				$('#btnaddrpt').click(function(event){
			
					var lbtst = document.getElementById('test').value;
					if(lbtst=="") {
						alert("First select the report!");
					}else {
						var answer = confirm("Do you want to proceed with \""+lbtst+"\" test?");
						lbtst=lbtst+".php";
						if(answer==true) {
							window.location.replace(lbtst);
						}
					}
				});



				function showdata(id) { 
						var btnval = document.getElementById(id);
						 var $row = $(btnval).closest("tr");
						 $tds1 = $row.find("td:nth-child(2)");//sltid
						window.location.replace("showbloodreport.php?sltid="+$tds1.text()+"");
				}
		</script>	
	</body>
</html>			
