<?php
	session_start();
	$filter = $_GET['fltr'];
	$conn = new mysqli("localhost","root","","project");

	if($conn->connect_error) {
		die("Connection failed".$conn->connect_error);
	}

	$return_arr = array();
	$x=1;
	$patregid=$_COOKIE['ptntregid'];
	$username=$_COOKIE['un'];

	if($filter=="MyName"){
		$sqlselectsymptrem="select  ds.sltdt, ps.symptname,ps.remarks, dr.drnm
							from doctorslot ds, doctor dr, ptntsymptom ps
							where  dr.usrname = '$username' 
							and ds.drrgnID= dr.drrgnID
							and ds.ptnrgnID=$patregid
							and ds.status in ('I','C')	
							and ps.drsltID = ds.drsltID		
							order by ds.drsltID desc";

		$result1 = $conn->query($sqlselectsymptrem);

		if (!$result1) {
	    trigger_error('Invalid query: ' . $conn->error);
		}

		if ($result1->num_rows > 0) {
	       while($obj = $result1->fetch_object()) {
	       		$return_arr[$x]["sltdt"] = $obj->sltdt;
		       	$return_arr[$x]["sympnm"] = $obj->symptname;
		       	$return_arr[$x]["rem"] = $obj->remarks;
		       	$return_arr[$x]["drnm"] = $obj->drnm;
		       	$x++;
	    	}
	    }

	    $myJSON = json_encode($return_arr);

		print_r($myJSON);
	} else if($filter=="MySpeciality") {
				$sqlselectsymptrem="select  ds.sltdt, ps.symptname,ps.remarks, dr.drnm
							from doctorslot ds, doctor dr, ptntsymptom ps 
							where dr.drspeclty=(select drspeclty from doctor where usrname='$username')		
							and ds.drrgnID= dr.drrgnID
							and ds.ptnrgnID=$patregid
							and ds.status in ('I','C')	
							and ps.drsltID = ds.drsltID
							order by ds.drsltID desc";

		$result1 = $conn->query($sqlselectsymptrem);

		if (!$result1) {
	    trigger_error('Invalid query: ' . $conn->error);
		}

		if ($result1->num_rows > 0) {
	       while($obj = $result1->fetch_object()) {
	       		$return_arr[$x]["sltdt"] = $obj->sltdt;
		       	$return_arr[$x]["sympnm"] = $obj->symptname;
		       	$return_arr[$x]["rem"] = $obj->remarks;
		       	$return_arr[$x]["drnm"] = $obj->drnm;
		       	$x++;
	    	}
	    }

	    $myJSON = json_encode($return_arr);

		print_r($myJSON);
	}

?>