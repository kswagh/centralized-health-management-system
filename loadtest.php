<?php
	session_start();

	$conn = new mysqli("localhost","root","","project");
	if($conn->connect_error) {
		die("Connection failed".$conn->connect_error);
	}

	$patregid=$_COOKIE['ptntregid'];

	$sqlselectpres="select ds.drsltID,ds.sltdt,ds.sltm,pt.lbtstname,dr.drnm
				from doctorslot ds,ptntlabtest pt,doctor dr
				where ds.drsltID=pt.drsltID
				and ds.status in ('I','C')
				and ds.drrgnID=dr.drrgnID
				and ds.ptnrgnID=$patregid order by ds.drsltID desc";
	$result1 = $conn->query($sqlselectpres);
	if (!$result1) {
    trigger_error('Invalid query: ' . $conn->error);
	}

	$return_arr = array();
	$x=1;
	if ($result1->num_rows > 0) {
       while($obj = $result1->fetch_object()) {
       		$return_arr[$x]["drsltid"] = $obj->drsltID;
       		$return_arr[$x]["sltdt"] = $obj->sltdt;
       		$return_arr[$x]["sltm"] = $obj->sltm;
	       	$return_arr[$x]["lbtstname"] = $obj->lbtstname;
	       	$return_arr[$x]["drnm"] = $obj->drnm;
	       	$x++;
    	}
    }

    $myJSON = json_encode($return_arr);

	print_r($myJSON);


?>