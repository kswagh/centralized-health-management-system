<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<script src="jquery-3.3.1.min.js" >
		</script>

		<link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> 
	 
		<script src="js/bootstrap.min.js">
		</script>

		<style type="text/css">

		  	/*for nav bar*/
		  	body {
		  		margin: 0;
			  font-family: Arial, Helvetica, sans-serif;
			}
			/*for footer*/
			.content {
			  min-height: 100%;
			}
			/*symp/pres/bill.... tabs*/
			.nav-tabs>li>a {
			 color: #ffffff  ;
			}
			.nav>li>a:focus, .nav>li>a:hover{
			    background-color: #1c2331;
			}
			.nav-tabs{
			    background-color: #33b5e5; 
			}

			/*card*/
			.card{
				margin-top: 10px;
				margin-bottom: 20px;
			}

			/*table*/
			.row{
			    /*margin-top:40px;*/
			    padding: 0 10px;
			}
			.clickable{
			    cursor: pointer;   
			}

			.panel-heading div {
				margin-top: -18px;
				font-size: 15px;
			}
			.panel-body{
				display: none;
			}  

			.thread-dark{

			}
		</style>
	<head>	
	<body>

		<!-- first navbar -->
		<nav class="navbar navbar-inverse" style="background-color: 2F4F4F;">
	 		<ul class="navbar-nav">
    			<li class="nav-item" >
    				<a class="navbar-brand" href="#">
						<img src="img/healthcarelogo.jpg" alt="Logo" style="width:40px;">
					</a>
		    		<a href="#home" >LUPUS HEALTHCARE</a>
		  		</li>
		  	</ul>
		  	<ul class="nav navbar-nav navbar-right">	
		  		<li class="nav-item">		
			   		<a style="color: white;"><?php $uname=$_COOKIE['un']; echo $uname; ?></a>
					<button class="btn btn-warning btn-sm" onclick="logout()" >Logout</button>
				</li>
			</ul>		
		 </nav>

		 <!-- second navbar -->
	 	<nav class="navbar navbar-inverse">
	 		<ul class="navbar-nav">
    			<li class="nav-item">
    				<button onclick="bcktotests()" class="btn btn-default btn-sm">Back To Tests</button>
    			</li>
    		</ul>
    	</nav>	



		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12">
					<div class="card">
						<header class="card-header card-header-danger" style="background-color : 2F4F4F;">	
							<h3 class="card-title" style="color: white;">Blood Test Details</h3>
						</header>
							<article class="card-body">			
									<div class="form-group">
										<div class="col-6"> 
											<label>Blood Group: </label><label id="bldgrpid"></label><br/>	
										</div>										
											<div class="col-12">
												<div class="table-responsive">
												<table  class="table ">
													<thead class="">
														<th>#</th>
														<th>Test</th>
														<th>Result</th>
														<th>Unit</th>
														<th>Biological Reference Interval</th>
													</thead>
													<tbody>
														<tr>
															<td>1</td>
															<td><label>Haemoglobin:</label></td>
															<td><label id="hemid"></label></td>
															<td><label>g/dL</label></td>
															<td><label>13.0-18.0</label></td>
														</tr>
														<tr>
															<td>2</td>
															<td><label>R.B.C Count</label></td>
															<td><label id="rbccntid"></label></td>
															<td><label>10^6/uL</label></td>
															<td><label>4.00-6.00</label></td>
														</tr>
														<tr>
															<td>3</td>
															<td><label>Platelet Count:</label></td>
															<td><label id="platcntid"></label></td>
															<td><label>x10^3/uL</label></td>
															<td><label>150-450</label></td>
														</tr>
														<tr>
															<td>4</td>
															<td><label>W.B.C Count:</label></td>
															<td><label id="wbccntid"></label></td>
															<td><label>per cu-mm</label></td>
															<td><label>4000-11000</label></td>
														</tr>
													</tbody>	
												</table>
											</div>	
											</div>
											<div class="col-6">				
												<label>REMARKS:</label><br/>
											</div>
											<div class="col-12">
												<textarea id="remid" rows="2" cols="50" class="form-control" readonly></textarea>
											</div>							
									</div>
							</article>
						</div>
					</div>
				</div>
			</div>					

	<footer class="footer position-relative page-footer font-small blue " style="background-color: 2F4F4F; ">
	  <div class="footer-copyright text-center py-3">© 2018 Copyright:
	    <a href="https://mdbootstrap.com/education/bootstrap/"> Lupus Healthcare</a>
	  </div>
	</footer>		

	</footer>		
		<script>

			function logout() {
				if (confirm('Are you sure you want to logout?')) {
					window.location.replace("patientdestroysession.php");
				}					 
			}

			function bcktotests() {
				window.location.replace("patienttst.php");	
			}

			
			loadreports();
			/*load reports in the table*/
			function loadreports() {
					var sid = '<?php echo $_GET['sltid']; ?>';
					$.ajax({
							type: "GET",
							url: "getbloodreports.php",
							data: {sltid: sid},
							success: function(response){
								var testdet = JSON.parse(response);
								$.each(testdet, function(i,data) {
							      $('#bldgrpid').text(data.bldgrp);
							      $('#hemid').text(data.hem);
							      $('#rbccntid').text(data.rbccnt);
							      $('#platcntid').text(data.pltcnt);
							      $('#wbccntid').text(data.wbccnt);
							      $('#remid').val(data.rem);
							});
						}	
					});
			}



		</script>	
	</body>
</head>		