<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<script src="jquery-3.3.1.min.js" >
		</script>

		<link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> 
	 
		<script src="js/bootstrap.min.js">
		</script>
		<style>
			 	/*for nav bar*/
		  	body {
		  		margin: 0;
			  font-family: Arial, Helvetica, sans-serif;
			}
			/*footer*/
			.content {
			  min-height: 100%;
			}
			/*symp/pres/bill.... tabs*/
			.nav-tabs>li>a {
			 color: #ffffff  ;
			}
			.nav>li>a:focus, .nav>li>a:hover{
			    background-color: #1c2331;
			}
			.nav-tabs{
			    background-color: #33b5e5; 
			}

			/*card*/
			.card{
				margin-top: 10px;
				margin-bottom: 20px;
			}

			/*table*/
			.row{
			   /* margin-top:40px;*/
			    padding: 0 10px;
			}
			.clickable{
			    cursor: pointer;   
			}

			.panel-heading div {
				margin-top: -18px;
				font-size: 15px;
			}
			.panel-body{
				display: none;
			}  

			.thread-dark{

			}
		</style>	
	</head>	
	<body>

		<div class="content">
		<!-- first navbar -->
		<nav class="navbar navbar-inverse" style="background-color: 2F4F4F;">
	 		<ul class="navbar-nav">
    			<li class="nav-item" >
    				<a class="navbar-brand" href="#">
						<img src="img/healthcarelogo.jpg" alt="Logo" style="width:40px;">
					</a>
		    		<a href="#home" >LUPUS HEALTHCARE</a>
		  		</li>
		  	</ul>
		  	<ul class="nav navbar-nav navbar-right">	
		  		<li class="nav-item">		
			   		<a style="color: white;"><?php $uname=$_COOKIE['un']; echo $uname; ?></a>
					<button class="btn btn-warning btn-sm" onclick="logout()" >Logout</button>
				</li>
			</ul>		
		 </nav>	

		<!-- nav tabs -->	
		<ul class="nav nav-tabs nav-justified">	
		  <li class="nav-item "><a class="nav-link " href="patienthomepage.php">Book Appointments</a></li>
		  <li class="nav-item"><a class="nav-link active" href="patientsymp.php">Symptoms</a></li>
		  <li class="nav-item"><a class="nav-link " href="patienttst.php">Test Reports</a></li>
		  <li class="nav-item"><a class="nav-link " href="patientdiag.php">Diagnosis</a></li>
		  <li class="nav-item"><a class="nav-link " href="patientpres.php">Prescription</a></li>
		  <li  class="nav-item" ><a class="nav-link " href="patientpay.php">Billing</a></li>
	 	</ul> 
		

		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-12">
					<div class="card">
						<div class="card-body">			
							<!-- <div class="panel-heading">
								<h3 class="panel-title">Symptoms</h3>
							</div> -->
							<div class="table-responsive">
							<table class="table table-hover table-striped" id="tabfillsymp">
								<thead class="thead-dark">
									<tr>
										<th>#</th>
										<th>Date</th>
										<th>Symptom</th>
										<th>Remarks</th>
										<th>Doctor</th>
									</tr>
								</thead>
							</table>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>								
	</div>
	<footer class="footer page-footer font-small blue " style="background-color: 2F4F4F; ">
	  <div class="footer-copyright text-center py-3">© 2018 Copyright:
	    <a href="https://mdbootstrap.com/education/bootstrap/"> Lupus Healthcare</a>
	  </div>
	</footer>		

	<script>
		loadsymp();

		function logout() {
				window.location.replace("patientdestroysession.php");				 
			}

		function bcktoaptmts() {
			window.location.replace("patienthomepage.php");	
		}
		function loadsymp(){
			$.ajax({
						type: "GET",
						url: "loadsymp.php",				
						success: function(response){
						    var sympdet = JSON.parse(response);
						        $.each(sympdet, function(i,data) {;					
						            $("#tabfillsymp").append("<tr><td>"+i+"</td><td>" + data.sltdt + "</td><td>"+data.sympnm+"</td><td>"+data.rem+"</td><td>"+data.drnm+"</td></tr><br/>");
						        });
						}
					});
		}

		/*	$(document).on("click", 'ul li', function(){
			    $('ul li').removeClass('active');
			    $(this).addClass('active');
			});*/
	</script>
	</body>	
</html>		