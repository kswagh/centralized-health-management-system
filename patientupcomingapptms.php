<?php
	session_start();

	$conn = new mysqli("localhost","root","","project");

	if($conn->connect_error) {
		die("Connection failed".$conn->connect_error);
	}

	$return_arr = array();
	$x=1;
	$patregid=$_COOKIE['ptntregid'];
	$sql="select drsltID,drnm,drspeclty,sltdt,sltm from doctorslot,doctor
		 where doctorslot.drrgnID=doctor.drrgnID 
		 and status='Y'
		 and ptnrgnID=$patregid
		 and sltdt>=CURDATE() order by sltdt asc";

	$result1 = $conn->query($sql);

	if (!$result1) {
    	trigger_error('Invalid query: ' . $conn->error);
	}

	if ($result1->num_rows > 0) {
       while($obj = $result1->fetch_object()) {
       		$return_arr[$x]["drsltid"] = $obj->drsltID;
       		$return_arr[$x]["drnm"] = $obj->drnm;
	       	$return_arr[$x]["drspeclty"] = $obj->drspeclty;
	       	$return_arr[$x]["sltdt"] = $obj->sltdt;
	       	$return_arr[$x]["sltm"] = $obj->sltm;
	       	$x++;
    	}
    }

    $myJSON = json_encode($return_arr);

	print_r($myJSON);


?>