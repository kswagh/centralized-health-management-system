<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<script src="jquery-3.3.1.min.js" >
		</script>

		<link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> 
	 
		<script src="js/bootstrap.min.js">
		</script>

		<style type="text/css">

		  	/*for nav bar*/
		  	body {
		  		margin: 0;
			  font-family: Arial, Helvetica, sans-serif;
			}
			.content {
			  min-height: 100%;
			}
			/*symp/pres/bill.... tabs*/
			.nav-tabs>li>a {
			 color: #ffffff  ;
			}
			.nav>li>a:focus, .nav>li>a:hover{
			    background-color: #1c2331;
			}
			.nav-tabs{
			    background-color: #33b5e5; 
			}

			/*card*/
			.card{
				margin-top: 10px;
				margin-bottom: 20px;
			}

			/*table*/
			.row{
			    /*margin-top:40px;*/
			    padding: 0 10px;
			}
			.clickable{
			    cursor: pointer;   
			}

			.panel-heading div {
				margin-top: -18px;
				font-size: 15px;
			}
			.panel-body{
				display: none;
			}  

			.thread-dark{

			}

		</style>	
	<head>	
	<body>
		<div class="content">
	<!-- first navbar -->
		<nav class="navbar navbar-inverse" style="background-color: 2F4F4F;">
	 		<ul class="navbar-nav">
    			<li class="nav-item" >
    				<a class="navbar-brand" href="#">
						<img src="img/healthcarelogo.jpg" alt="Logo" style="width:40px;">
					</a>
		    		<a href="#home" >LUPUS HEALTHCARE</a>
		  		</li>
		  	</ul>
		  	<ul class="nav navbar-nav navbar-right">	
		  		<li class="nav-item">		
			   		<a style="color: white;"><?php $uname=$_COOKIE['un']; echo"Dr ".$uname; ?></a>
					<button class="btn btn-warning btn-sm" onclick="logout()" >Logout</button>
				</li>
			</ul>		
		 </nav>
		  
		 <!-- nav tabs -->	
		<ul class="nav nav-tabs nav-justified">
		  <li class="nav-item"><a class="nav-link " href="docpatientsymp.php">Symptoms</a></li>
		  <li class="nav-item"><a class="nav-link " href="docpatienttst.php">Test Reports</a></li>
		  <li class="nav-item"><a class="nav-link " href="docpatientdiag.php">Diagnosis</a></li>
		  <li class="nav-item"><a class="nav-link active" href="docpatientpres.php">Prescription</a></li>
		  <li  class="nav-item" ><a class="nav-link " href="docpatientpay.php">Billing</a></li>
	 	</ul> 

	 	<!-- second navbar -->
	 	<nav class="navbar navbar-inverse">
	 		<ul class="navbar-nav">
    			<li class="nav-item">
    				<button onclick="bcktoaptmts()" class="btn btn-default btn-sm">Back To Appointments</button>
    			</li>
    		</ul>
    		<ul class="nav navbar-nav navbar-right">	
    			<li class="nav-item" >
    				<button class="btn btn-light btn-sm">Patient Name: <?php $patname = $_COOKIE['patname']; echo $patname; ?></button>	
    			</li>
    		</ul>
    	</nav>	

		
			<!-- prescription and table  cards-->
			<div class="container">
				<div class="row">
				  	<div class="col-sm-5">
				    	<div class="card">
				      		<div class="card-body">
				      			<div class="form-group"><!-- dot put row in the class as, it is used to print elements side by side -->
										<!-- to print elements side by side, give 'div form group row' and then give 'div class col ' in it  -->
										<div class="col-6">
											<label><b>Medicine:</b></label><br/>
										</div>
										<div class="col-7">	
											<select id="drg" class="form-control" >
												<option value="" selected disabled hidden>Choose Medicine</option>
											</select>
										</div>
										<div class="col-12">	
											<input type="text" name="txtdrg" id="drgid" class="form-control" >
										</div>	
										<div class="col-6">
											<label><b>Dosage:</b></label><br/>
										</div>
										<div class="col-7">		
											<select id="dsg" class="form-control" >
												<option value="" selected disabled hidden>Choose Dosage</option>
											</select>
										</div>						
										<div class="col-12">	
											<input type="text" name="txtdsg" id="dsgid" class="form-control" >
										</div>
										<div class="row">
											<div class="col-6">
												<button type="button" id="btnsave"  class="btn btn-primary btn-md">Save</button>&nbsp;				
											</div>
											<div class="col-6">
												<button type="button" id="btnprint"  class="btn btn-primary btn-md">Print Prescription</button>	
											</div>	
										</div>	
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-7">
				    	<div class="card">
				    		<header class="card-header card-header-danger" >	
								<h3 class="card-title" >Past Prescription Record</h3>
							</header>
				      		<div class="card-body">
				      		<div class="table-responsive">								
								<table class="table table-hover table-striped" id="tabfillpres">
									<thead class="thead-dark">
										<tr>
											<th>Date</th>
											<th>Medicine Name</th>
											<th>Dosage</th>
											<th>Doctor</th>
										</tr>
									</thead>	
									<tbody>
										
									</tbody>
								</table>
							</div>	
							</div>
						</div>
					</div>											
				</div>
			</div>
			</div>								
	
	<footer class="footer page-footer font-small blue position-relative" style="background-color: 2F4F4F; ">
	  <div class="footer-copyright text-center py-3">© 2018 Copyright:
	    <a href="https://mdbootstrap.com/education/bootstrap/"> Lupus Healthcare</a>
	  </div>
	</footer>
		
		<script>
			loadselectdrug();
			loadselectdosage();
			loadtabpres();


			function logout() {
				if (confirm('Are you sure you want to logout?')) {
					window.location.replace("destroysession.php");
				}					 
			}
			function bcktoaptmts() {
				window.location.replace("clearcookiesbktoaptmt.php");	
			}

			/*load drugs select dropdown*/
			function loadselectdrug() {
					$.ajax({
						type: "GET",
						url: "getdrugfilled.php",				
						success: function(response){
							var select = document.getElementById('drg');
						    var drugdet = JSON.parse(response);
						        $.each(drugdet, function(i,data) {
						    		newOption = document.createElement('option');
								    newOption.value=data.prefix + data.nm + data.power;
								    newOption.text=data.prefix + data.nm + data.power;
								    select.appendChild(newOption);    	
						        });
						}
					});

			}

			//fill dosage
			function loadselectdosage() {
					$.ajax({
						type: "GET",
						url: "getdosagefilled.php",				
						success: function(response){
						    var select = document.getElementById('dsg');
						    var dosagedet = JSON.parse(response);
						        $.each(dosagedet, function(i,data) {
						    		newOption = document.createElement('option');
								    newOption.value=data.dsg;
								    newOption.text=data.dsg;
								    select.appendChild(newOption);    	
						        });
						}
					});
			}


			//onload fill table with past prescription records
			function loadtabpres() {	

					$.ajax({
						type: "GET",
						url: "loadpres.php",				
						success: function(response){
						    var presdet = JSON.parse(response);
						        $.each(presdet, function(i,data) {				
						            $("#tabfillpres").append("<tr><td>" + data.sltdt + "</td><td>"+data.drgnm+"</td><td>"+data.dsg+"</td><td>"+data.drnm+"</td></tr><br/>");
						        });
						}
					});
			}
		
				$('#drg').on('change',function(event){
					 var info = $('#drg').find('option:selected').text();
  					$("#drgid").val(info);
				});

				$('#dsg').on('change',function(event){
					//alert($('#symp').find('option:selected').text());
					//$('#txtsym').val("")
					 var info = $('#dsg').find('option:selected').text();
  					$("#dsgid").val(info);
				});

				$('#btnsave').click(function(event){
					$('#tabfillpres').find("tr:gt(0)").remove();
					var drg = document.getElementById('drgid').value;
					var dsg = document.getElementById('dsgid').value;
					if(drg.match(/^[a-zA-Z][a-zA-Z\s0-9]*$/) && dsg.match(/^[a-zA-Z0-9][a-zA-Z\s0-9/&:,-]*$/))
					{
						var answer = confirm("Do you want to save Prescription details?");
							if(answer==true) {
								$.ajax({
									type: "GET",
									url: "savepres.php",
									data: {txtdrg: drg, txtdsg: dsg},
									success: function(response){
										
									    var presdet = JSON.parse(response);
									    
									        $.each(presdet, function(i,data) {
									            $("#tabfillpres").append("<tr><td>" + data.sltdt + "</td><td>"+data.drgnm+"</td><td>"+data.dsg+"</td><td>"+data.drnm+"</td></tr><br/>");
									        });

									        $('#drgid').val('');
									        $('#dsgid').val('');
									}
								});
							} else {
								loadtabpres();
							}
					} else {
						alert("Invalid Fields: Medicine or Dosage!")
					}		
				});

				$('#btnprint').click(function(event){
						var drug = document.getElementById('drgid').value;
						var dosage = document.getElementById('dsgid').value;
						var restorepage = document.body.innerHTML;
				        var printcontent = "<table style='column-gap: 100px'><th>Medicine</th><th>Dosage</th><tr><td>"+drug+"</td><td>"+dosage+"</td><tr></table>";
				        document.body.innerHTML = printcontent;
				        window.print();
				        document.body.innerHTML=restorepage;
				});	
		</script>	
	</body>
</html>			
