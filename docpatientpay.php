<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<script src="jquery-3.3.1.min.js" >
		</script>

		<link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> 
	 
		<script src="js/bootstrap.min.js">
		</script>

		<style type="text/css">
				/*for nav bar*/
		  	body {
		  		margin: 0;
			  font-family: Arial, Helvetica, sans-serif;
			}
			.content {
			  min-height: 100%;
			}
			/*symp/pres/bill.... tabs*/
			.nav-tabs>li>a {
			 color: #ffffff  ;
			}
			.nav>li>a:focus, .nav>li>a:hover{
			    background-color: #1c2331;
			}
			.nav-tabs{
			    background-color: #33b5e5; 
			}

			/*card*/
			.card{
				margin-top: 10px;
				margin-bottom: 20px;
			}

			/*table*/
			.row{
			    /*margin-top:40px;*/
			    padding: 0 10px;
			}
			.clickable{
			    cursor: pointer;   
			}

			.panel-heading div {
				margin-top: -18px;
				font-size: 15px;
			}
			.panel-body{
				display: none;
			}  

			.thread-dark{

			}
		</style>	
	<head>	
	<body>
		<div class="content">
			<nav class="navbar navbar-inverse" style="background-color: 2F4F4F;">
	 		<ul class="navbar-nav">
    			<li class="nav-item" >
    				<a class="navbar-brand" href="#">
						<img src="img/healthcarelogo.jpg" alt="Logo" style="width:40px;">
					</a>
		    		<a href="#home" >LUPUS HEALTHCARE</a>
		  		</li>
		  	</ul>
		  	<ul class="nav navbar-nav navbar-right">	
		  		<li class="nav-item">		
			   		<a style="color: white;"><?php $uname=$_COOKIE['un']; echo"Dr ".$uname; ?></a>
					<button class="btn btn-warning btn-sm" onclick="logout()" >Logout</button>
				</li>
			</ul>		
		 </nav>
		  
		 <!-- nav tabs -->	
		<ul class="nav nav-tabs nav-justified">
		  <li class="nav-item"><a class="nav-link " href="docpatientsymp.php">Symptoms</a></li>
		  <li class="nav-item"><a class="nav-link " href="docpatienttst.php">Test Reports</a></li>
		  <li class="nav-item"><a class="nav-link " href="docpatientdiag.php">Diagnosis</a></li>
		  <li class="nav-item"><a class="nav-link " href="docpatientpres.php">Prescription</a></li>
		  <li  class="nav-item" ><a class="nav-link active" href="docpatientpay.php">Billing</a></li>
	 	</ul> 

	 	<!-- second navbar -->
	 	<nav class="navbar navbar-inverse">
	 		<ul class="navbar-nav">
    			<li class="nav-item">
    				<button onclick="bcktoaptmts()" class="btn btn-default btn-sm">Back To Appointments</button>
    			</li>
    		</ul>
    		<ul class="nav navbar-nav navbar-right">	
    			<li class="nav-item" >
    				<button class="btn btn-light btn-sm">Patient Name: <?php $patname = $_COOKIE['patname']; echo $patname; ?></button>	
    			</li>
    		</ul>
    	</nav>	

    	<!-- payment input and table fill -->
    	<div class="container">
				<div class="row">
				  	<div class="col-sm-5">
				    	<div class="card">
				      		<div class="card-body">
				      			<div class="form-group">
				      				<div class="col-6">	
										<label><b>Consultation Fee:</b></label>
									</div>
									<div class="col-12">	
										<input type="text" name="txtconfee"  id="consfeeid" class="form-control"><br/>
									</div>
									<div class="col-6">	
										<label><b>Other Fee:</b></label>
									</div>
									<div class="col-12">	
										<input type="text"  name="txtfollfee" id="follfeeid" class="form-control">
									</div><br>
									<div class="col-6">	
										<label><b>Total:</b></label>
									</div>
									<div class="col-12">
										<input type="text" name="txttot" id="totid" class="form-control">
									</div>
									<div class="col-5">	
										<button type="button" id="calctot" class="btn  btn-sm">Calulate Total</button><br/><br/>
									</div>		
									<div class="col-6">	
										<label><b>Total Paid:</b></label><br/>
									</div>
									<div class="col-12">	
										<input type="text" name="txttotpd"  id="totpaidid" class="form-control"><br/>
									</div>
									<div class="col-6">	
										<label><b>Balance:</b></label><br/>
									</div>
									<div class="col-12">
										<input type="text" name="txtbal"  id="balid" class="form-control">	
									</div>	
									<div class="col-5">	
										<button type="button" id="calcbal" class="btn btn-sm">Calulate Balance</button><br/><br/>
									</div>	
									<div class="col-6">	
										<label><b>Cheque Number:</b></label><br/>
									</div>	
									<div class="col-12">
										<input type="text" name="txtchqno" id="chqid" class="form-control"><br/><br>
									</div>
									<div class="row">
										<div class="col-6">	
											<button type="button" id="btnsave" class="btn btn-primary btn-md">Save</button>	
										</div>
										<div class="col-6">	
											<button type="button" onclick="sendmsg()" class="btn btn-primary btn-md">Send Bill Details</button>	
										</div>
									</div>		
								</div>
							</div>
						</div>
					</div>			
						<div class="col-sm-7">
					    	<div class="card">
					    		<header class="card-header card-header-danger" >	
									<h3 class="card-title" >Past Payment Record</h3>
								</header>
					      		<div class="card-body">
					      		<div class="table-responsive">			
									<table class="table table-hover table-striped" id="tabfillpay">
										<thead class="thead-dark">
											<tr>
												<th>#</th>			
												<th>Date</th>
												<th>Total Amount</th>
												<th>Balance</th>
												<th>Doctor</th>
											</tr>
										</thead>
										<tbody>
											
										</tbody>	
									</table>
								</div>	
								</div>
							</div>
						</div>
				</div>
			</div>
			</div>									
		
	<footer class="footer page-footer font-small blue position-relative" style="background-color: 2F4F4F; ">
	  <div class="footer-copyright text-center py-3">© 2018 Copyright:
	    <a href="https://mdbootstrap.com/education/bootstrap/"> Lupus Healthcare</a>
	  </div>
	</footer>	

		<script>

			loadtabpay();

			function logout() {
				if (confirm('Are you sure you want to logout?')) {
					window.location.replace("destroysession.php");
				}					 
			}
			function bcktoaptmts() {
				window.location.replace("clearcookiesbktoaptmt.php");	
			}

			function sendmsg() {
				var tot = document.getElementById("totid").value;
				var bal = document.getElementById("balid").value;
				var link =  "https://api.whatsapp.com/send?phone=&text="  ;
				var res = link + "LUPUS HEALTHCARE %0D%0A Total Bill Amount:"+tot+"%0D%0A Balance:"+bal;
					  
				var win = window.open(res, '_blank');
 				win.focus(); 
			}
	

			/*load diagnosis select dropdown*/
			
			//onload fill table with past symptom records
			function loadtabpay() {	
					$.ajax({
						type: "GET",
						url: "loadpay.php",				
						success: function(response){
						    var paydet = JSON.parse(response);
						        $.each(paydet, function(i,data) {					
						            $("#tabfillpay").append("<tr><th>"+i+"</th><td>" + data.sltdt + "</td><td>"+data.totpay+"</td><td>"+data.bal+"</td><td>"+data.drnm+"</td></tr><br/>");
						        });
						}
					});
			}
		
				/*calculate total*/
				$('#calctot').click(function(event){
					var confee = Number(document.getElementById('consfeeid').value);
					var follfee = Number(document.getElementById('follfeeid').value);
					var calc = confee+follfee
					$('#totid').val(calc);
				});

				$('#calcbal').click(function(event){
					var tot = Number(document.getElementById('totid').value);
					var totpd = Number(document.getElementById('totpaidid').value);
					var calc=tot-totpd
					$('#balid').val(calc);
				});

				$('#btnsave').click(function(event){
					$('#tabfillpay').find("tr:gt(0)").remove();
					var confee = document.getElementById('consfeeid').value;
					var follfee = document.getElementById('follfeeid').value;
					var tot = document.getElementById('totid').value;
					var totpd = document.getElementById('totpaidid').value;
					var bal = document.getElementById('balid').value;
					var chqno = document.getElementById('chqid').value;
						/*if(follfee==""){
							alert();
									follfee=0;
							}
							if(confee==""){
								alert();
									confee=0;
							}*/
					if(confee.match(/^[0-9]{0,4}$/) && follfee.match(/^\d{0,4}$/) && tot.match(/^[0-9]{0,4}$/) && totpd.match(/^[0-9]{0,4}$/) &&bal.match(/^[0-9-]?[0-9]{0,4}$/)&& chqno.match(/^[a-zA-Z0-9]*$/)) {	
						if(tot==""||totpd=="") {
							alert("Fields empty! First fill all fields.");
						} else {

							var answer = confirm("Do you want to save payment details?");
								if(answer==true) {
									if(follfee==""){
											follfee=0;
									}
									if(confee==""){
										alert();
											confee=0;
									}
									$.ajax({
										type: "GET",
										url: "savepay.php",
										data: {txtconfee: confee, txtfollfee: follfee, txttot:tot, txttotpd:totpd, txtbal:bal, txtchqno:chqno},
										success: function(response){

										    var paydet = JSON.parse(response);
										        $.each(paydet, function(i,data) {
										            $("#tabfillpay").append("<tr><th>"+i+"</th><td>" + data.sltdt + "</td><td>"+data.totpay+"</td><td>"+data.bal+"</td><td>"+data.drnm+"</td></tr><br/>");
										        });

										       $('#consfeeid').val('');
										       $('#follfeeid').val('');
										       $('#totid').val('');
										       $('#totpaidid').val('');
										       $('#balid').val('');
										       $('#chqid').val('');
										}
									});
								} else {
									loadtabpay();
								}
						}	
					} else {
						alert("Invalid Fields!");
					}	
				});
		</script>	
	</body>
</html>			
