 <?php
	$uname=$_COOKIE['un'];
	echo"<h3>Welcome ".$uname."</h3>";
?>
<html>
	<head>
		<script src="jquery-3.3.1.min.js">
		</script>
	<head>	
	<body>
		<!--Main Tabs-->	
		<a href="docpatientsympdiag.php">Symptoms/diadnosis</a> &nbsp; <a>Test Reports</a> &nbsp; <a>Prescription</a> &nbsp; <a>Billing</a> <br>
		
		<button onclick="logout()">Logout</button>
		
		<div>
			<!-- <form action="savesympdiag.php"> -->
					<label>Symptoms/Complaints:</label><br/>
					<textarea name="txtsym" rows="2" cols="50" id="txtsymp"></textarea>
					<select id="symp" >
						<option value="" selected disabled hidden>Choose Symptom</option>
					</select><br>
					<label>Diagnosis:</label><br/>
					<textarea name="txtdia" rows="2" cols="50" id="txtdiag"></textarea>
					<select id="diag" >
						<option value="" selected disabled hidden>Choose Diagnosis</option>
					</select><br>					
					<label>Remarks:</label><br/>
					<textarea name="txtremark" rows="2" cols="25" id="txtrem"></textarea><br>

				<button type="button" id="btnsave">Save</button>	
				<!-- <input type="submit" value="Save"> -->
			<!-- </form> -->	
		</div>
		<div>	
			<table id="tabfilldiag">
				<tr>
					<th>Date</th>
					<th>Symptom</th>
					<th>Diagnisis</th>
					<th>Doctor</th>
				</tr>
			</table>	
		</div>
		<script>
			loadselectsymp();
			loadselectdiag();
			loaddiagsymppast();

			function logout() {
				window.location.replace("destroysession.php");				 
			}

			function loadselectsymp() {
				var obj = new XMLHttpRequest();
				obj.open("GET", "getsympfilled.php", true);
				obj.send();

				obj.onreadystatechange = function() {
					if(obj.status == 200 && obj.readyState == 4) {
						var myObj = JSON.parse(obj.responseText);
						
						
						var select = document.getElementById('symp');
						
						for (let sympname in myObj) {
							//new Option('myObj[sympname]','myObj[sympname]');
							newOption = document.createElement('option');
						    newOption.value=myObj[sympname];
						    newOption.text=myObj[sympname];
						    select.appendChild(newOption);

					
							/*document.getElementById("docselect").innerHTML = "<option value=\""+myObj[docname]+"\">"+myObj[docname]+"</option>";*/
						}
					}

				}
			}

			function loadselectdiag() {
				var obj = new XMLHttpRequest();
				obj.open("GET", "getdiagfilled.php", true);
				obj.send();

				obj.onreadystatechange = function() {
					if(obj.status == 200 && obj.readyState == 4) {
						var myObj = JSON.parse(obj.responseText);
			
						
						var select = document.getElementById('diag');
						
						for (let diagname in myObj) {
							//new Option('myObj[diagname]','myObj[diagname]');
							
							newOption = document.createElement('option');
						    newOption.value=myObj[diagname];
						    newOption.text=myObj[diagname];
						    select.appendChild(newOption);

					
							/*document.getElementById("docselect").innerHTML = "<option value=\""+myObj[docname]+"\">"+myObj[docname]+"</option>";*/
						}
					}

				}
			}

			//onload fill table with past symptom records
			function loaddiagsymppast() {	
					$.ajax({
						type: "GET",
						url: "loadsympdiag.php",				
						success: function(response){
							//alert("hi");
						    var sympdiagdet = JSON.parse(response);
						        $.each(sympdiagdet, function(i,data) {
						        	//alert("hi");					
						            $("#tabfilldiag").append("<tr><td>" + data.sltdt + "</td><td>"+data.sympnm+"</td><td>"+data.diagnm+"</td><td>"+data.drnm+"</td></tr><br/>");
						        });
						}
					});
			}
		
				$('#symp').on('change',function(event){
					//alert($('#symp').find('option:selected').text());
					//$('#txtsym').val("")
					 var info = $('#symp').find('option:selected').text();
  					$("#txtsymp").append(info+"\n");
				});

				$('#diag').on('change',function(event){
					//alert($('#symp').find('option:selected').text());
					//$('#txtsym').val("")
					 var info = $('#diag').find('option:selected').text();
  					$("#txtdiag").append(info+"\n");
				});

				$('#btnsave').click(function(event){
					$('#tabfilldiag').find("tr:gt(0)").remove();
					var symp = document.getElementById("txtsymp").value;
					var diag = document.getElementById('txtdiag').value;
					var rem = document.getElementById('txtrem').value;

					var answer = confirm("Do you want to save Symptom and Diagnosis details?");
						if(answer==true) {
							$.ajax({
								type: "GET",
								url: "savesympdiag.php",
								data: {txtsym: symp, txtdia: diag, txtremark:rem},
								success: function(response){
									alert("Record saved!");
/*
							    	$("#tabslots").append("<tr><th>"+"Date"+"</th><th>"+"Symptoms"+"</th><th>"+"Diagnosis"+"</th><th>"+"Doctor"+"</th></tr>");*/

								    var sympdiagdet = JSON.parse(response);
								        $.each(sympdiagdet, function(i,data) {
								            $("#tabfilldiag").append("<tr><td>" + data.sltdt + "</td><td>"+data.sympnm+"</td><td>"+data.diagnm+"</td><td>"+data.drnm+"</td></tr><br/>");
								        });
								}
							});
						}
				});
		</script>	
	</body>
</html>			
