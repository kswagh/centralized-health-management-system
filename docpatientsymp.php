<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<script src="jquery-3.3.1.min.js" >
		</script>

		<link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> 
	 
		<script src="js/bootstrap.min.js">
		</script>

		<style type="text/css">

		  	/*for nav bar*/
		  	body {
		  		margin: 0;
			  font-family: Arial, Helvetica, sans-serif;
			}
			/*footer*/
			.content {
			  min-height: 100%;
			}
			/*symp/pres/bill.... tabs*/
			.nav-tabs>li>a {
			 color: #ffffff  ;
			}
			.nav>li>a:focus, .nav>li>a:hover{
			    background-color: #1c2331;
			}
			.nav-tabs{
			    background-color: #33b5e5; 
			}

			/*card*/
			.card{
				margin-top: 10px;
				margin-bottom: 20px;
			}

			/*table*/
			.row{
			   /* margin-top:40px;*/
			    padding: 0 10px;
			}
			.clickable{
			    cursor: pointer;   
			}

			.panel-heading div {
				margin-top: -18px;
				font-size: 15px;
			}
			.panel-body{
				display: none;
			}  

			.thread-dark{

			}



		</style>	
	</head>	
	<body>
		<div class="content">
		<!-- first navbar -->
		<nav class="navbar navbar-inverse" style="background-color: 2F4F4F;">
	 		<ul class="navbar-nav">
    			<li class="nav-item" >
    				<a class="navbar-brand" href="#">
						<img src="img/healthcarelogo.jpg" alt="Logo" style="width:40px;">
					</a>
		    		<a href="#home" >LUPUS HEALTHCARE</a>
		  		</li>
		  	</ul>
		  	<ul class="nav navbar-nav navbar-right">	
		  		<li class="nav-item">		
			   		<a style="color: white;"><?php $uname=$_COOKIE['un']; echo"Dr ".$uname; ?></a>
					<button class="btn btn-warning btn-sm" onclick="logout()" >Logout</button>
				</li>
			</ul>		
		 </nav>
		  
		 <!-- nav tabs -->	
		<ul class="nav nav-tabs nav-justified">
		  <li class="nav-item"><a class="nav-link active" href="docpatientsymp.php">Symptoms</a></li>
		  <li class="nav-item"><a class="nav-link " href="docpatienttst.php">Test Reports</a></li>
		  <li class="nav-item"><a class="nav-link " href="docpatientdiag.php">Diagnosis</a></li>
		  <li class="nav-item"><a class="nav-link " href="docpatientpres.php">Prescription</a></li>
		  <li  class="nav-item" ><a class="nav-link " href="docpatientpay.php">Billing</a></li>
	 	</ul> 

	 	<!-- second navbar -->
	 	<nav class="navbar navbar-inverse">
	 		<ul class="navbar-nav">
    			<li class="nav-item">
    				<button onclick="bcktoaptmts()" class="btn btn-default btn-sm">Back To Appointments</button>
    			</li>
    		</ul>
    		<ul class="nav navbar-nav navbar-right">	
    			<li class="nav-item" >
    				<button class="btn btn-light btn-sm">Patient Name: <?php $patname = $_COOKIE['patname']; echo $patname; ?></button>	
    			</li>
    		</ul>
    	</nav>	

    							
			<!-- symptoms and table  cards-->
			<div class="container">
				<div class="row">
				  	<div class="col-sm-5">
				    	<div class="card">
				      		<div class="card-body">
				      			<div class="form-group"><!-- dot put row in the class as, it is used to print elements side by side -->
										<!-- to print elements side by side, give 'div form group row' and then give 'div class col ' in it  -->
										<div class="col-6">
											<label><b>Symptoms/Complaints:</b></label><br/>
										</div>	
										<div class="col-7">
											<select id="symp" class="form-control" >
												<option value="" selected disabled hidden>Choose Symptom</option>
											</select>
										</div>
										<div class="col-12">
											<textarea name="txtsymp" rows="2" cols="50" id="sympid" class="form-control" ></textarea>	
										</div>
										<div class="col-6">
											<label><b>Remarks:</b></label><br/>
										</div>
										<div class="col-8">	
											<textarea id ="txtremid" name="txtrem" rows="2" cols="25" id="remid" class="form-control"></textarea><br/>
										</div>
										<div class="col-6">	
											<button type="button" id="btnsave" class="btn btn-primary btn-md">Save</button>
										</div>
								</div>			
							</div>	
						</div>
					</div>
					<div class="col-sm-7">
				    	<div class="card">
				    		<header class="card-header card-header-danger" >	
								<h3 class="card-title" >Past Symptom Record</h3>
							</header>
				      		<div class="card-body">
				      					<div class="col-7">
											<select id="filterselectid" class="form-control" onchange="loadfilter()">
												<option value="" selected disabled hidden>Choose Filter</option>
												<option value="">All</option>
												<option value="">MyName</option>
 												<option	value="">MySpeciality</option>
											</select>
										</div>
										<div class="table-responsive">				
										<table class="table table-hover table-striped" id="tabfillsymp">
											<thead class="thead-dark">
											<tr>
												<th >#</th>
												<th>Date</th>
												<th>Symptom</th>
												<th>Remarks</th>
												<th>Doctor</th>
											</tr>
											</thead>
											<tbody>
												
											</tbody>
										</table>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>																				

			<footer class="footer page-footer font-small blue " style="background-color: 2F4F4F; ">
			  <div class="footer-copyright text-center py-3">© 2018 Copyright:
			    <a href="https://mdbootstrap.com/education/bootstrap/"> Lupus Healthcare</a>
			  </div>
			</footer>


			
		<script>
			loadselectsymp();
			loadtabsymp();

			function logout() {
				if (confirm('Are you sure you want to logout?')) {
					window.location.replace("destroysession.php");
				}					 
			}

			function bcktoaptmts() {
				window.location.replace("clearcookiesbktoaptmt.php");	
			}
	

			//fill symptom select tag with masters data
			function loadselectsymp() {
				var obj = new XMLHttpRequest();
				obj.open("GET", "getsympfilled.php", true);
				obj.send();

				obj.onreadystatechange = function() {
					if(obj.status == 200 && obj.readyState == 4) {
						var myObj = JSON.parse(obj.responseText);
						
						
						var select = document.getElementById('symp');
						
						for (let sympname in myObj) {
							//new Option('myObj[sympname]','myObj[sympname]');
							newOption = document.createElement('option');
						    newOption.value=myObj[sympname];
						    newOption.text=myObj[sympname];
						    select.appendChild(newOption);

					
							/*document.getElementById("docselect").innerHTML = "<option value=\""+myObj[docname]+"\">"+myObj[docname]+"</option>";*/
						}
					}

				}
			}	
			/*function for filter*/
			function loadfilter(){
				 var filter = $('#filterselectid').find('option:selected').text();
				 if(filter==="All") {
				 	$('#tabfillsymp').find("tr:gt(0)").remove();
				 	$.ajax({
						type: "GET",
						url: "loadsymp.php",				
						success: function(response){
							//console.log(response.responseText);
						    var sympdet = JSON.parse(response);
						    //console.log(JSON.stringify(sympdet));
						        $.each(sympdet, function(i,data) {
						            $("#tabfillsymp").append("<tr><th>"+i+"</th><td>" + data.sltdt + "</td><td>"+data.sympnm+"</td><td>"+data.rem+"</td><td>"+data.drnm+"</td></tr><br/>");
						        });
						}
					});
				 } else if (filter==="MyName") {
				 	$('#tabfillsymp').find("tr:gt(0)").remove();
				 	$.ajax({
						type: "GET",
						url: "loadfiltersymp.php",
						data: {fltr: filter},				
						success: function(response){
							//console.log(response.responseText);
						    var sympdet = JSON.parse(response);
						        $.each(sympdet, function(i,data) {
						            $("#tabfillsymp").append("<tr><th>"+i+"</th><td>" + data.sltdt + "</td><td>"+data.sympnm+"</td><td>"+data.rem+"</td><td>"+data.drnm+"</td></tr><br/>");
						        });
						}
					});
				 } else if (filter==="MySpeciality") {
				 	$('#tabfillsymp').find("tr:gt(0)").remove();
				 	$.ajax({
						type: "GET",
						url: "loadfiltersymp.php",
						data: {fltr: filter},				
						success: function(response){
							//console.log(response.responseText);
						    var sympdet = JSON.parse(response);
						        $.each(sympdet, function(i,data) {
						            $("#tabfillsymp").append("<tr><th>"+i+"</th><td>" + data.sltdt + "</td><td>"+data.sympnm+"</td><td>"+data.rem+"</td><td>"+data.drnm+"</td></tr><br/>");
						        });
						}
					});
				 }					 
			}		

			//onload fill table with past symptom records
			function loadtabsymp() {	
					$.ajax({
						type: "GET",
						url: "loadsymp.php",				
						success: function(response){
							//console.log(response.responseText);
						    var sympdet = JSON.parse(response);
						    console.log(JSON.stringify(sympdet));
						        $.each(sympdet, function(i,data) {

						            $("#tabfillsymp").append("<tr><th>"+i+"</th><td>" + data.sltdt + "</td><td>"+data.sympnm+"</td><td>"+data.rem+"</td><td>"+data.drnm+"</td></tr><br/>");
						        });
						}
					});
			}
		
				$('#symp').on('change',function(event){
					//alert($('#symp').find('option:selected').text());
					//$('#txtsym').val("")
					 var info = $('#symp').find('option:selected').text();
  					$("#sympid").append(info+"\n");
				});

				//to save symptom record and append that ecord to the table
				$('#btnsave').click(function(event){
					var rem = document.getElementById("txtremid").value;
					var symp = document.getElementById("sympid").value;
				
					if(rem.match(/^[a-zA-Z\s]*$/) && symp.match(/^[a-zA-Z][a-zA-Z\s0-9/&:,-]*$/))
					{
						$('#tabfillsymp').find("tr:gt(0)").remove();
						//var symp = document.getElementById("sympid").value;
						//var rem = document.getElementById('remid').value;
						if(symp=="" ) {
							alert("Fields empty! First fill all fields.");

						} else {

							var answer = confirm("Do you want to save Symptom details?");
								if(answer==true) {

									$.ajax({
										type: "GET",
										url: "savesymp.php",
										data: {txtsymp: symp, txtrem:rem},
										success: function(response){

										    var sympdet = JSON.parse(response);
										        $.each(sympdet, function(i,data) {
										
										            $("#tabfillsymp").append("<tr><th>"+i+"</th><td>" + data.sltdt + "</td><td>"+data.sympnm+"</td><td>"+data.rem+"</td><td>"+data.drnm+"</td></tr><br/>");
										        });

										        $('#sympid').val('');
										        $('#remid').val('');
										}
									});
								} else {
									loadtabsymp();
								}
							}
						}else {

							alert("Invalid fields-Symptom or Remarks:pattern[a-zA-Z]" );
					}	
				});
		</script>	
	</body>
</html>			
